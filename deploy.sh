#!/usr/bin/env bash

zola build
cp -R public/* pages/
cp .domains pages/
cp LICENSE pages/
cd pages
git add -A
git commit -m "$(git status --porcelain)"
git push
cd ..
