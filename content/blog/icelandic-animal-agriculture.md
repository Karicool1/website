+++
title = "‘Pfft, there is no animal abuse in Icelandic agriculture’"
date = "2023-03-07"
authors = ["Kári"]

[extra]
intro = "When considering the production of animal products in Iceland, we often imagine images of lambs in a field and cows in a barn. Despite the belief that instances of animal abuse only occur in distant countries, there have been documented cases of mistreatment within Iceland's agriculture industry. Both Icelandic and international news outlets regularly report on these incidents."
+++


When we think of the production of animal products in Iceland, we picture cute little lambs in a field with their mom and happy cows in a cozy barn. All the animal abuse we see footage of and read about on the news only happens in other countries far away, but not here of course... right? 

The truth is that there are many records of animal abuse in the agriculture industry in Iceland and both Icelandic and foreign news reporters expose them regularly. Below this article is a list of Icelandic articles about animal abuse of various farm animals in Iceland. 

Even though we do not personally witness this animal abuse it does not mean we don't take part in it. By buying animal products in stores and restaurants we are financing this devastating industry. Things like pigs getting stuffed into gas chambers and suffocated or hens being kept in cages so small that they can't stretch their wings are still a reality because people want to be able to buy cheap meat, eggs, and dairy products in Iceland. 

However, as consumers we have the power to object to animal abuse by choosing to eat plant-based food and refusing to pay for animal products.

## Resources

### Examples of animal cruelty in Icelandic agriculture

- **Egg laying hens**: [The chickens suffered because of overcrowding - photos - RÚV.is (ruv.is)](https://www.ruv.is/frettir/innlent/haenurnar-thjadust-vegna-threngsla-myndir)
- **Blood mares**: [Ignite - The Red Gold | RÚV Television (ruv.is)](https://www.ruv.is/sjonvarp/spila/kveikur/35249/ag54gp)
- **Pigs**: [Icelandic pigs in too narrow stalls - RÚV.is (ruv.is)](https://www.ruv.is/frettir/innlent/islensk-svin-a-alltof-throngum-basum )
- **Chickens**: [Icelandic chickens still in cages which are banned in Europe - RÚV.is (ruv.is)](https://www.ruv.is/frettir/innlent/2023-01-18-islenskar-haenur-enn-i-burum-sem-eru-bonnud-i-evropu)
- **Pig farmers steal government subsidies**: [Pay dividends after accepting government subsidies to improve the condition of pigs - Heimildin](https://heimildin.is/grein/16526/)
- **Pigs in gas chambers**: [Sing to the pigs before they go to the gas chamber - Vísir (visir.is)](https://www.visir.is/g/20232429660d/sungid-fyrir-svinin-adur-en-thau-fara-i-gas-klefann)
- **Lousy farmed salmon**: ["They are eaten alive" (mbl.is)](https://www.mbl.is/frettir/innlent/2023/11/02/their_eru_etnir_lifandi/)
- **Cows**: [Dairy farm fined and license revoked (mbl.is)](https://www.mbl.is/frettir/innlent/2023/05/22/mjolkurbu_sektad_og_svipt_leyfi/)
- **Whales**: [Fishing suspended until MAST and Fiskistofa have verified improvements - RÚV.is (ruv.is)](https://nyr.ruv.is/frettir/innlent/2023-09-14-veidarnar-stodvadar-thar-til-mast-og-fiskistofa-hafa-sannreynt-urbaetur-391682)
