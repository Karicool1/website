+++
title = "“Iss, það er ekkert dýraníð í íslenskum landbúnaði.”"
date = "2023-03-07"
authors = ["Kári"]

[extra]
intro = "Þegar litið er til framleiðslu á dýraafurðum á Íslandi ímyndum við okkur oft myndir af lömbum á túni og kúm í hlöðu. Margt fólk heldur að dýraníð  eigi sér aðeins stað í fjarlægum löndum en það koma reglulega upp slík mál í íslenskum landbúnaði. Bæði íslenskir ​​og erlendir fréttamiðlar greina reglulega frá þessum atvikum."
+++


Þegar við hugsum um íslenska framleiðslu dýraafurða sjáum við fyrir okkur lítil, sæt lömb úti í haga með mömmu sinni og hamingjusamar kýr í huggulegu fjósi. Öll þessi myndbönd og fréttir af dýraníði gerast bara í öðrum löndum langt í burtu, ekki hér á landi að sjálfsögðu. Eða hvað? 

Sannleikurinn er sá að það eru margar heimildir um dýraníð í landbúnaði á Íslandi og það er reglulega fjallað um það í íslenskum og erlendum fjölmiðlum. Fyrir neðan þessa grein er listi af íslenskum fréttum sem fjalla um dýraníð ýmissa húsdýra á Íslandi. 

Þótt að við verðum ekki sjálf vitni að dýraníði þýðir ekki að við sem neytendur tökum ekki þátt í því. Með því að kaupa dýraafurðir í búðum eða á veitingastöðum erum við að fjármagna þennan sorglega iðnað. Aðferðir eins og að troða svínum í gasklefa og kæfa þau, eða að geyma hænur í svo litlum búrum að þær geta ekki rétt úr sér er enn veruleiki vegna þess að fólk vill geta keypt ódýrt kjöt, egg og mjólkurvörur á Íslandi. 

Sem neytendur getum við hins vegar mótmælt dýraníði með því að velja að kaupa frekar plöntumiðað fæði. 


## Hlekkir

### Dæmi um dýraníð í íslenskum landbúnaði

- **Brúneggjamálið**: [Hænurnar þjáðust vegna þrengsla - myndir - RÚV.is (ruv.is)](https://www.ruv.is/frettir/innlent/haenurnar-thjadust-vegna-threngsla-myndir)
- **Blóðmerar**: [Kveikur - Rauða gullið | RÚV Sjónvarp (ruv.is)](https://www.ruv.is/sjonvarp/spila/kveikur/35249/ag54gp)
- **Svín**: [Íslensk svín á alltof þröngum básum - RÚV.is (ruv.is)](https://www.ruv.is/frettir/innlent/islensk-svin-a-alltof-throngum-basum)
- **Hænur**: [Íslenskar hænur enn í búrum sem eru bönnuð í Evrópu - RÚV.is (ruv.is)](https://www.ruv.is/frettir/innlent/2023-01-18-islenskar-haenur-enn-i-burum-sem-eru-bonnud-i-evropu)
- **Svínabændur stela ríkisstyrkjum**: [Greiddu sér arð eftir að hafa þegið ríkisstyrk til að bæta aðbúnað svína - Heimildin](https://heimildin.is/grein/16526/)
- **Svín í gasklefum**: [Sungið fyrir svínin áður en þau fara í gasklefann - Vísir (visir.is)](https://www.visir.is/g/20232429660d/sungid-fyrir-svinin-adur-en-thau-fara-i-gas-klefann)
- **Lúsugur eldislax**: [„Þeir eru étnir lifandi“ (mbl.is)](https://www.mbl.is/frettir/innlent/2023/11/02/their_eru_etnir_lifandi/)
- **Kýr**: [Mjólkurbú sektað og svipt leyfi (mbl.is)](https://www.mbl.is/frettir/innlent/2023/05/22/mjolkurbu_sektad_og_svipt_leyfi/)
- **Hvalir**: [Veiðarnar stöðvaðar þar til MAST og Fiskistofa hafa sannreynt úrbætur - RÚV.is (ruv.is)](https://nyr.ruv.is/frettir/innlent/2023-09-14-veidarnar-stodvadar-thar-til-mast-og-fiskistofa-hafa-sannreynt-urbaetur-391682)
